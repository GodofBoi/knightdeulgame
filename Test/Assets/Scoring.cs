﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Scoring : MonoBehaviour
{
    public float totalPlayers;
    private float[] players = new float[4];

    public Transform scoreManager;
    public GameObject scorePrefab;

    public float civiPointGain;
    public float knightPointGain;
    public float kingPointGain;

    float slayCivi = 5;
    float slayKnight = 10;
    float slayKing = 50;
    float knightStun = 1;

    UnityEvent scoreEvent;

    public float score;

    public Text scoreText;

    public List<PlayerStats> allPlayerStats;

    // Start is called before the first frame update
    void Start()
    {

        print(players.Length);
        //scoreEvent.AddListener(Score);

        float xAxis = 100;

        for (totalPlayers = 0; totalPlayers < players.Length; totalPlayers++)
        {
            print("spawned score");
            GameObject myNewScore = Instantiate(scorePrefab, new Vector3(xAxis, 125, 0), Quaternion.identity);
            myNewScore.transform.SetParent(scoreManager.transform);

            xAxis += +200;
        }

        

        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown("space"))
        {
            int total = 0;
            foreach (PlayerStats stats in allPlayerStats)
            {
                total += stats.GetTotalScore();
            }
            Debug.Log(total);

        }
    }

    public GameObject Test()
    {
        return null;
    }

    IEnumerator Score()
    {
        for(; ; )
        {
            foreach (PlayerStats individualPlayer in allPlayerStats)
            {
                //individualPlayer.AddScore(individualPlayer.myPlayer.scorePerTick);

            }
            yield return new WaitForSeconds(1f);
        }
        
    }

 
    public void KingSlayed()
    {
       
    }

    public void AddScore(int PlayerID, int ScoreToAdd)
    {
        allPlayerStats[PlayerID].AddScore(ScoreToAdd);
    }
}

//Class for player stats
[System.Serializable]
public class PlayerStats
{
    /*
     * -Holds a reference to the player its tracking 
     * -Keeps track of scores (number of knights killed, kings, etc)
     * 
     * 
     * */

    //Player reference
    public PlayerPrefs myPlayer;

    public int pointsGained;
    public int knightsSlayed;
    public int civisSlayed;
    public int kingsSlayed;

    public int GetTotalScore()
    {
        return pointsGained;
    }

    public void AddScore(int amount)
    {
        pointsGained += amount;
    }
}


