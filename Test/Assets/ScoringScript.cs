﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoringScript : MonoBehaviour
{
    public float civiPointGain;
    public float knightPointGain;
    public float kingPointGain;

    float slayCivi = 5;
    float slayKnight = 10;
    float slayKing = 50;
    float knightStun = 1;

    float player1Score;
    float player1TotalScore;

    string stringFormatPlayer1Score = "####";
    public string Player1ScorePrefix;

    public bool player1Knight;
    public bool player1King;
    public bool player1Civi;

    float player2Score;
    float player2TotalScore;

    string stringFormatPlayer2Score = "####";
    public string Player2ScorePrefix;

    public bool player2Knight;
    public bool player2King;
    public bool player2Civi;

    float player3Score;
    float player3TotalScore;

    string stringFormatPlayer3Score = "####";
    public string Player3ScorePrefix;

    public bool player3Knight;
    public bool player3King;
    public bool player3Civi;

    float player4Score;
    float player4TotalScore;

    string stringFormatPlayer4Score = "####";
    public string Player4ScorePrefix;

    public bool player4Knight;
    public bool player4King;
    public bool player4Civi;

    public Text player1ScoreText;
    public Text player2ScoreText;
    public Text player3ScoreText;
    public Text player4ScoreText;

    // Start is called before the first frame update
    void Start()
    {
        player1ScoreText.color = Color.red;
        player2ScoreText.color = Color.blue;
        player3ScoreText.color = Color.green;
        player4ScoreText.color = Color.yellow;

    }

    // Update is called once per frame
    void Update()
    {
        player1ScoreText.text = Player1ScorePrefix + player1Score.ToString(stringFormatPlayer1Score);
        player2ScoreText.text = Player2ScorePrefix + player2Score.ToString(stringFormatPlayer2Score);
        player3ScoreText.text = Player3ScorePrefix + player3Score.ToString(stringFormatPlayer3Score);
        player4ScoreText.text = Player4ScorePrefix + player4Score.ToString(stringFormatPlayer4Score);

        player1ScoreUpdate();
        player2ScoreUpdate();
        player3ScoreUpdate();
        player4ScoreUpdate();

    }

    void player1ScoreUpdate()
    {
        if(player1Civi == true)
        {
            player1Score += civiPointGain * Time.deltaTime;
        }

        if(player1Knight == true)
        {
            player1Score += knightPointGain * Time.deltaTime;
        }

        if(player1King == true)
        {
            player1Score += kingPointGain * Time.deltaTime;
        }

        if (Input.GetKeyDown("space"))
        {
            player1Score += slayCivi;
            player1Score += slayKnight;
            player1Score += slayKing;
            player1Score += knightStun;
        }
    }

    void player2ScoreUpdate()
    {
        if (player2Civi == true)
        {
            player2Score += civiPointGain * Time.deltaTime;
        }

        if (player2Knight == true)
        {
            player2Score += knightPointGain * Time.deltaTime;
        }

        if (player2King == true)
        {
            player2Score += kingPointGain * Time.deltaTime;
        }

        if (Input.GetKeyDown("space"))
        {
            player2Score += slayCivi;
            player2Score += slayKnight;
            player2Score += slayKing;
            player2Score += knightStun;

        }
    }

    void player3ScoreUpdate()
    {
        if(player3Civi == true)
        {
            player3Score += civiPointGain * Time.deltaTime;
        }

        if(player3Knight == true)
        {
            player3Score += knightPointGain * Time.deltaTime;
        }

        if(player3King == true)
        {
            player3Score += kingPointGain * Time.deltaTime;
        }

        if (Input.GetKeyDown("space"))
        {
            player3Score += slayCivi;
            player3Score += slayKnight;
            player3Score += slayKing;
            player3Score += knightStun;

        }
    }

    void player4ScoreUpdate()
    {
        if (player4Civi == true)
        {
            player4Score += civiPointGain * Time.deltaTime;
        }

        if (player4Knight == true)
        {
            player4Score += knightPointGain * Time.deltaTime;
        }

        if (player4King == true)
        {
            player4Score += kingPointGain * Time.deltaTime;
        }

        if (Input.GetKeyDown("space"))
        {
            player4Score += slayCivi;
            player4Score += slayKnight;
            player4Score += slayKing;
            player4Score += knightStun;

        }
    }
}
