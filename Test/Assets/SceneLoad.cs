﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoad : MonoBehaviour
{
    public static SceneLoad instance;



    public string[] gameScenes;

    private void Awake()
    {
        //Don't destroy the game manager object between scenes
        if(instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        
    }

    private void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        

    }

    public void StartGame()
    {
        Debug.Log("Game scene has been loaded");
        SceneManager.LoadScene(gameScenes[1], LoadSceneMode.Single);
    }


}
