﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public float min = 0;
    public float max = 255;

    public Image fadePanelImage;
    public GameObject fadePanelObject;
    private Color fade;

    public GameObject MainMenuScreen;
    public GameObject CreditsMenuScreen;
    public GameObject SelectPlayerScreen;

    public Button playButton;
    public Button credits;
    public Button back;
    public Button exit;

    bool fadeOn;


    // Start is called before the first frame update
    void Start()
    {

        playButton.onClick.AddListener(StartGame);
        exit.onClick.AddListener(ExitGame);
        credits.onClick.AddListener(Credits);
        back.onClick.AddListener(BackToMainMenu);

        MainMenuScreen.SetActive(true);
        CreditsMenuScreen.SetActive(false);
        SelectPlayerScreen.SetActive(false);
        fadePanelObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
       //fadePanelImage.material.color = fade;
        fadePanelImage.color = fade;

        if(fadeOn == true)
        {
            //    fade.a = Mathf.Lerp(min, max, .5f * Time.deltaTime);
            fade.a += 0.1f * Time.deltaTime;
        }
        
    }

    IEnumerator Fade()
    {
        fadePanelObject.SetActive(true);
        fadeOn = true;
        yield return new WaitForSeconds(2);
        Debug.Log("Game scene has been loaded");
        SceneManager.LoadScene("GameScreen", LoadSceneMode.Single);
        yield return null;
    }

    public void Credits()
    {
        CreditsMenuScreen.SetActive(true);
        MainMenuScreen.SetActive(false);
    }

    public void BackToMainMenu()
    {
        CreditsMenuScreen.SetActive(false);
        MainMenuScreen.SetActive(true);
    }

    public void StartGame()
    {
        StartCoroutine("Fade");     
    }

    public void ExitGame()
    {
        Application.Quit();
        print("end");
    }
}
