﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
    private float timer;
    public float startTime;
    public float roundNumber;

    public float countdown;

    public Text timerText;
    public Text roundText;

    string stringFormatTime = "##";
    public string timePrefix;

    string stringFormatRound = "##";
    public string roundPrefix;

    public GameObject winnersScreen;
    public Image FadeInScreen;

    public Button nextRound;

    bool roundOver;
    bool fadeOff = true;

    // Start is called before the first frame update
    void Start()
    {
        timer = startTime;
        winnersScreen.SetActive(false);
        nextRound.onClick.AddListener(RoundBegin);
    }

    // Update is called once per frame
    void Update()
    {
        timerText.text = timer.ToString();

        if(timer >= 0)
        {
            timer = timer - 1f * Time.deltaTime;
        }

        timerText.text = timePrefix + timer.ToString(stringFormatTime);

        roundText.text = roundPrefix + roundNumber.ToString(stringFormatRound);

        if (fadeOff == true)
        {
            if (FadeInScreen.color.a > 0)
            {
                FadeInScreen.color = new Color(0, 0, 0, FadeInScreen.color.a - 1f * Time.deltaTime);
            }

            else
            {
                FadeInScreen.gameObject.SetActive(false);
                fadeOff = false;
            }
        }

        RoundTimer();
    }

    public void RoundBegin()
    {
        timer = startTime;
        winnersScreen.SetActive(false);
        roundOver = false;

    }

    void RoundTimer()
    {
        if(timer <= 10)
        {
            timerText.color = Color.red;
        }

        if(timer <= 0)
        {
            if(roundOver == false)
            {
                timer = 0;
                winnersScreen.SetActive(true);

                roundOver = true;
                roundNumber += +1;
            }
           
        }
    }
}
