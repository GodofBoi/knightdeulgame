﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameScreen : MonoBehaviour
{
    
    public Button titleReturn;
    public Button exit;

    // Start is called before the first frame update
    void Start()
    {
        
        titleReturn.onClick.AddListener(TitleReturn);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TitleReturn()
    {
        Debug.Log("Game scene has been loaded");
        SceneManager.LoadScene("TitleScreen", LoadSceneMode.Single);
    }



    public void ExitGame()
    {
        Application.Quit();
        print("end");
    }
}
